#ifndef BLEINTERFACE_H
#define BLEINTERFACE_H

#include <QObject>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothDeviceInfo>
#include <QLowEnergyController>
#include <QDebug>
#include <QLoggingCategory>
#include <QThread>
#include <QTimer>

class BleInterface : public QObject {
    Q_OBJECT

    Q_PROPERTY(bool connected READ isConnected NOTIFY connectedChanged)

public:
    BleInterface();
    void init();

public slots:
    bool connectDevice(QString name);
    void startDiscovery();
    void setStreamEnabled(QLowEnergyService *service, QLowEnergyDescriptor *descr, bool enabled);
    bool disconnectDevice(QString address);
    void sendData(QByteArray data);

    void disconnectAllDevices();
    void saveParameters();
    void setParameters(float p, float i, float d, float setpoint);
private:
    QMap<QBluetoothUuid, QLowEnergyService*> foundServices;
    QTimer connectTimer;
    QString bleDeviceDirectConnect = "";

    QBluetoothDeviceDiscoveryAgent discoveryAgent;
    QMap<QString, QBluetoothDeviceInfo> btDevices;

    QStringList getDevicesNames() const;
    bool isConnected();
    bool serviceUserDataFound = false;
    bool connected = false;
    quint32 bytesCount = 0;
    quint32 bytesCountTotal = 0;
    double getRxTotal();
    QSet<QBluetoothUuid> uuidListChars;
    QSet<QBluetoothUuid> uuidListServices;

    struct ControllerInstance {
        QLowEnergyController *controller;
        QList<QLowEnergyService*> services;
    };
    QMap<QBluetoothAddress, ControllerInstance*> bleControllers;

    void connectService(QLowEnergyController *controller, QBluetoothUuid serviceUUID);
    void setStreamsEnabled(QLowEnergyService *service, QBluetoothUuid uuid, bool enabled);
    float bufferToFloat(const char *buffer);
    void appendValue(float value, QByteArray *array);
private slots:
    void deviceFound(const QBluetoothDeviceInfo &info);
    void scanFinished();
    void serviceDiscovered(const QBluetoothUuid &gatt);
    void serviceScanDone();
    void characteristicUpdated(const QLowEnergyCharacteristic &c, const QByteArray &value);
    void confirmedDescriptorWrite(const QLowEnergyDescriptor &d, const QByteArray &value);
    void setIsConnected(bool isConnected);
    void serviceChanged(QLowEnergyService::ServiceState s);
    void deviceConnected();


signals:
    void newValues(float temperature, float output);
    void newParameters(float p, float i, float d, float setpoint);
    void newData(const QByteArray &value);
    void bleDevicesChanged();
    void btDeviceNamesChanged();
    void connectedChanged();
    void ecgModeChanged(quint32 ecgMode);
    void newDeviceName(QString name);
};

#endif // BLEINTERFACE_H
