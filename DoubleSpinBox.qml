import QtQuick 2.15

import QtQuick.Controls 2.1


SpinBox {
    property int decimals: 1
    property real realValue: 0.0
    property real realFrom: 0.0
    property real realTo: 1000.0
    property real realStepSize: 0.1
    property real factor: Math.pow(10, decimals)
    id: spinbox

    signal editValue()

    stepSize: realStepSize*factor
    value: realValue*factor
    to : realTo*factor
    from : realFrom*factor
    validator: DoubleValidator {
        bottom: Math.min(spinbox.from, spinbox.to)*spinbox.factor
        top:  Math.max(spinbox.from, spinbox.to)*spinbox.factor
    }
    onFocusChanged: {
        console.log("has focus: " + focus);
        if(focus) {
            editValue(spinbox);
        }
    }
    textFromValue: function(value, locale) {
        return parseFloat(value*1.0/factor).toFixed(decimals);
    }
}

