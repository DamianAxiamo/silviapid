import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.15
import QtQuick.Window 2.2

import "."

ApplicationWindow {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("Silvia PID")

    property color paletteRed: "#cd0a20"
    property color paletteGreen: "#2acf6c"
    property color paletteBlue: "#0a84fe"

    Material.theme: Material.Dark
    Material.accent: Material.Cyan

    property real valueHeater: 0.0

    Component.onCompleted: {
        bleInterface.newParameters.connect(newParameters);
        bleInterface.newValues.connect(newValues);
    }

    onClosing: bleInterface.disconnectAllDevices();

    function newParameters(p, i, d, setpoint) {
        valueP.realValue = p.toFixed(1);
        valueI.realValue = i.toFixed(1);
        valueD.realValue = d.toFixed(1);
        valueSetpoint.realValue = setpoint.toFixed(1);
    }

    function newValues(temperature, output) {
        valueTemperature.text = temperature.toFixed(1) + "°C";
        valueHeater = output;
        plotter.newValues(temperature, output / 10.0, valueSetpoint.realValue);
    }

    function sendData() {
        bleInterface.setParameters(valueP.value / 10, valueI.value / 10, valueD.value / 10, valueSetpoint.value / 10);
    }

    header: Pane {
        width: parent.width
        height: 50
        Material.elevation: 2
        RowLayout {
            anchors.fill: parent
            Label {
                text: "Silvia PID"
                font.bold: true
                font.pixelSize: 16
                color: "cyan"
            }
            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
            Label {
                text: (bleInterface.connected ? "Connected" : "Not connected")
            }
        }
    }

    SwipeView {
        id: view
        currentIndex: 0
        anchors.fill: parent


        Item {
            ColumnLayout {
                anchors.margins: 10
                anchors.fill: parent
                GridLayout {
                    columns: 2

                    Label {
                        text: "Temperature"
                    }
                    Label {
                        id: valueTemperature
                    }
                    Label {
                        text: "Heater"
                    }
                    Rectangle {
                        Layout.fillWidth: true
                        Layout.preferredHeight: 20

                        border.color: "black"
                        border.width: 2
                        radius: 5

                        Rectangle {
                            anchors.margins: 2
                            anchors.left: parent.left
                            anchors.top: parent.top
                            height: parent.height - 2 * anchors.margins
                            width: (valueHeater / 1000.0) * (parent.width - 2 * anchors.margins)
                            color: root.paletteGreen
                            radius: 5
                        }
                        Label {
                            anchors.centerIn: parent
                            text: (valueHeater / 10).toFixed(0) + "%"
                        }
                    }
                }
                GridLayout {

                    columns: 3
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Label {
                        text: "P"
                    }
                    DoubleSpinBox {
                        id: valueP
                        onValueModified: sendData();
                        onEditValue: valueDialog.show(this)
                        realValue: 100.0
                    }
                    Button {
                        text: "save"
                        onClicked: bleInterface.saveParameters();
                        Layout.rowSpan: 4
                        Layout.fillHeight: true
                    }
                    Label {
                        text: "I"
                    }
                    DoubleSpinBox {
                        id: valueI
                        onValueModified: sendData();
                        onEditValue: valueDialog.show(this)
                        realValue: 0.0
                    }
                    Label {
                        text: "D"
                    }
                    DoubleSpinBox {
                        id: valueD
                        onValueModified: sendData();
                        onEditValue: valueDialog.show(this)
                        realValue: 100.0
                    }
                    Label {
                        text: "Set"
                    }
                    DoubleSpinBox {
                        id: valueSetpoint
                        onValueModified: sendData();
                        onEditValue: valueDialog.show(this)
                        realValue: 30.0
                    }
                }
            }
        }
        Plotter {
            id: plotter

        }
    }
    PageIndicator {
        count: view.count
        currentIndex: view.currentIndex
        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Dialog {
        id: valueDialog
        title: qsTr("Enter value")
        modal: true
        anchors.centerIn: parent

        standardButtons: Dialog.Ok | Dialog.Cancel

        property var spinBoxObject: null

        function show(spinBox) {
            spinBoxObject = spinBox;
            valueField.validator = spinBoxObject.validator;
            valueField.text = (spinBoxObject.value / 10).toFixed(1);
            valueDialog.open();
        }

        onAccepted: {
            spinBoxObject.realValue = valueField.text
            spinBoxObject.valueModified();
        }
        onClosed: spinBoxObject.focus = false;

        TextField {
            id: valueField
            selectByMouse: true
        }
    }
}
