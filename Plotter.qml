import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.15
import QtQuick.Window 2.2
import QtCharts 2.15

ChartView {
    id: chartView
    backgroundColor: "transparent"
    Layout.fillHeight: true
    Layout.fillWidth: true
    antialiasing: true
    x: -10
    y: -10

    margins.top: 0
    margins.bottom: 0
    margins.left: 0
    margins.right: 0
    backgroundRoundness: 0

    property int numValues: 0
    property int plotValues: 500



    function newValues(temperature, output, setpoint) {
        appendToSeries(tempSeries, temperature);
        appendToSeries(outputSeries, output);
        appendToSeries(setpointSeries, setpoint);
        numValues++;
    }

    function appendToSeries(series, value) {
        series.append(numValues, value);
        if(numValues > plotValues) {
            series.removePoints(0, 1);
        }
    }

    property color textColor: Material.foreground
    legend.labelColor: textColor

    ValueAxis {
        id: axisY1
        min: 10
        max: 150
        labelsFont.pixelSize: 12
        tickCount: 8
        labelFormat: "%i"
        labelsColor: textColor
    }
    ValueAxis {
        id: axisX1
        min: numValues - plotValues
        max: numValues - 1
        visible: false
    }
    LineSeries {
        id: setpointSeries
        axisY: axisY1
        axisX: axisX1
        color: root.paletteGreen
        width: 2
        name: qsTr("Setpoint ");
    }
    LineSeries {
        id: outputSeries
        axisY: axisY1
        axisX: axisX1
        color: root.paletteRed
        width: 2
        name: qsTr("Heater ");
    }
    LineSeries {
        id: tempSeries
        axisY: axisY1
        axisX: axisX1
        width: 2
        color: root.paletteBlue
        name: qsTr("Temperature ");
    }
}
