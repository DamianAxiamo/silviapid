#include "bleinterface.h"

BleInterface::BleInterface() {
    //QLoggingCategory::setFilterRules(QStringLiteral("qt.bluetooth* = true"));
    connect(&connectTimer, &QTimer::timeout, [&](){
        if(!isConnected()) {
            startDiscovery();
        }
    });
}

void BleInterface::init() {
    uuidListServices.insert(QBluetoothUuid::UserData);          //0x181c
    connectTimer.start(1000);
}


void BleInterface::saveParameters() {
    QByteArray cmd;
    cmd.append(0xA5);
    sendData(cmd);
}

void BleInterface::sendData(QByteArray data) {
    if(isConnected()) {
        QLowEnergyService *serviceUserData = foundServices.value(QBluetoothUuid::UserData);

        if(serviceUserData) {
            const QLowEnergyCharacteristic userDataCharacteristic = serviceUserData->characteristic(QBluetoothUuid::UserIndex); //0x2a9a
            serviceUserData->writeCharacteristic(userDataCharacteristic, data);
            qDebug() << "sent data: " + data.toHex();
        } else {
            qDebug() << "ERROR: BT connection not active!";
        }
    } else {
    }
}

void BleInterface::setParameters(float p, float i, float d, float setpoint) {
    QByteArray cmd;
    appendValue(p, &cmd);
    appendValue(i, &cmd);
    appendValue(d, &cmd);
    appendValue(setpoint, &cmd);
    sendData(cmd);
}

void BleInterface::appendValue(float value, QByteArray *array) {
    uint32_t temp;
    memcpy(&temp, &value, 4);
    array->push_back((temp >>  0) & 0xFF);
    array->push_back((temp >>  8) & 0xFF);
    array->push_back((temp >> 16) & 0xFF);
    array->push_back((temp >> 24) & 0xFF);
}

void BleInterface::startDiscovery() {
    discoveryAgent.stop();
    discoveryAgent.disconnect();
    btDevices.clear();
    discoveryAgent.setLowEnergyDiscoveryTimeout(5000);
#if defined (Q_OS_WINDOWS) || defined (Q_OS_ANDROID)
    connect(&discoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, this, &BleInterface::deviceFound);
#else
    connect(&discoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceUpdated, this, &BleInterface::deviceFound);
#endif
    connect(&discoveryAgent, &QBluetoothDeviceDiscoveryAgent::finished, this, &BleInterface::scanFinished);
    discoveryAgent.start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
    qDebug() << "started BLE discovery...";
}

bool BleInterface::connectDevice(QString name) {
    bool result = false;
    discoveryAgent.stop();

    if(btDevices.contains(name)) {
        QBluetoothDeviceInfo info = btDevices.value(name);
        QStringList entry;
        entry << name << info.address().toString();
        qDebug() << "connecting to BLE device: " << name << " with address: " << info.address();
        // whatever this is for...
        // Disconnect and delete old connection
        //disconnectDevice(info.address().toString());
        // Create new controller and connect it if device available
        ControllerInstance *i = new ControllerInstance;
        i->controller = QLowEnergyController::createCentral(info, this);
        bleControllers.insert(info.address(), i);
        connect(i->controller, &QLowEnergyController::serviceDiscovered, this, &BleInterface::serviceDiscovered);
        connect(i->controller, &QLowEnergyController::discoveryFinished, this, &BleInterface::serviceScanDone);
        connect(i->controller, &QLowEnergyController::connected, this, &BleInterface::deviceConnected);
        connect(i->controller, &QLowEnergyController::disconnected, this, [this]() {
            setIsConnected(false);
        });
        // Connect
        foundServices.clear();
        i->controller->connectToDevice();
        result = true;
    } else {
        qDebug() << "ERROR: device with name: " << name << " not found";
    }

    return result;
}

void BleInterface::deviceConnected() {
    QLowEnergyController *signalSender = static_cast<QLowEnergyController*>(sender());
    QBluetoothAddress address = signalSender->remoteAddress();
    ControllerInstance *bleDeviceControl = bleControllers.value(address);
    bleDeviceControl->controller->discoverServices();
}

void BleInterface::serviceDiscovered(const QBluetoothUuid &gatt) {
    QLowEnergyController *signalSender = static_cast<QLowEnergyController*>(sender());
    QBluetoothAddress address = signalSender->remoteAddress();
    qDebug() << "discovered service UUID " << gatt.toString() << " from " << address;

    if(uuidListServices.contains(gatt)) {
        qDebug() << "adding service " << gatt.toString();
        foundServices.insert(gatt, Q_NULLPTR);
    }
}

void BleInterface::setIsConnected(bool isConnected) {
    if(connected != isConnected) {
        connected = isConnected;
        emit connectedChanged();
        qDebug() << "BLE connected: " << isConnected;

        if(connected) {
        } else {
            startDiscovery();
        }
    }
}

void BleInterface::connectService(QLowEnergyController *controller, QBluetoothUuid serviceUUID) {
    QLowEnergyService *old = foundServices.value(serviceUUID);

    if(old != Q_NULLPTR) {
        old->disconnect();
        delete old;
        old = Q_NULLPTR;
    }

    QLowEnergyService *service = controller->createServiceObject(serviceUUID, this);

    if(service) {
        foundServices.insert(serviceUUID, service);
        connect(service, &QLowEnergyService::stateChanged, this, &BleInterface::serviceChanged);
        connect(service, &QLowEnergyService::descriptorWritten, this, &BleInterface::confirmedDescriptorWrite);
        qDebug() << "discovering characteristics of service " << serviceUUID.toString();
        service->discoverDetails();
    }
}

void BleInterface::serviceScanDone() {
    QLowEnergyController *signalSender = static_cast<QLowEnergyController*>(sender());
    QBluetoothAddress address = signalSender->remoteAddress();
    qDebug() << "finished service scan for device " << address.toString() << " got " << foundServices.count() << " services";

    foreach(QBluetoothUuid uuid, foundServices.keys()) {
        connectService(signalSender, uuid);
    }
}

double BleInterface::getRxTotal() {
    return bytesCountTotal / (1024.0 * 1024.0);
}

float BleInterface::bufferToFloat(const char *buffer) {
    float result = 0;
    memcpy(&result, buffer, 4);
    return result;
}

void BleInterface::characteristicUpdated(const QLowEnergyCharacteristic &c, const QByteArray &value) {
    int newBytes = value.length();

    switch(c.uuid().toUInt16()) {
        case QBluetoothUuid::UserIndex:
            bytesCount += newBytes;
            bytesCountTotal += value.length();

            //qDebug() << "bleInterface signal newData";
            if(newBytes > 0) {
                const char *data = value.data();

                if(newBytes == 24) {
                    float p = bufferToFloat(data + 0);
                    float i = bufferToFloat(data + 4);
                    float d = bufferToFloat(data + 8);
                    float setpoint = bufferToFloat(data + 12);
                    float temperature = bufferToFloat(data + 16);
                    float output = bufferToFloat(data + 20);
                    emit newParameters(p, i, d, setpoint);
                    emit newValues(temperature, output);
                    qDebug() << " P: " << p << " i: " << i << " D: " << d << " Setpoint: " << setpoint;
                    qDebug() << " Temperature: " << temperature << " Output: " << output;
                } else if(newBytes == 8) {
                    float temperature = bufferToFloat(data + 0);
                    float output = bufferToFloat(data + 4);
                    emit newValues(temperature, output);
                    qDebug() << " Temperature: " << temperature << " Output: " << output;
                }
            }

            break;
    }
}

void BleInterface::confirmedDescriptorWrite(const QLowEnergyDescriptor &d, const QByteArray &value) {
    QLowEnergyService *service = static_cast<QLowEnergyService*>(sender());

    if(d.isValid() && value == QByteArray::fromHex("0100")) {
        if(service == foundServices.value(QBluetoothUuid::UserData)) {
            // we successfully enabled notifications for USER_DATA service
            setIsConnected(true);
        }
    }

    if(service != Q_NULLPTR) {
        qDebug() << "written " << value.toHex() << " to service" << service->serviceUuid();
    }
}

void BleInterface::serviceChanged(QLowEnergyService::ServiceState s) {
    QLowEnergyService *service = static_cast<QLowEnergyService*>(sender());
    qDebug() << "serviceChanged state: " << s;

    switch(s) {
        case QLowEnergyService::ServiceDiscovered: {
            QList<QLowEnergyCharacteristic> chars = service->characteristics();
            qDebug() << "service: " << service->serviceName() << " found " << chars.size() << "characteristics";

            for(int i = 0; i < chars.size(); i++) {
                qDebug() << i << ":" << chars.at(i).name() << "uuid: " << chars.at(i).uuid();
                //if(uuidListChars.contains(chars.at(i).uuid())) {
                setStreamsEnabled(service, chars.at(i).uuid(), true);
                //}
            }

            break;
        }

        default:
            //nothing for now
            break;
    }
}

void BleInterface::disconnectAllDevices() {
    foreach(ControllerInstance *instance, bleControllers.values()) {
        if(instance != Q_NULLPTR && instance->controller->state() != QLowEnergyController::ControllerState::UnconnectedState) {
            instance->controller->disconnectFromDevice();
        }
    }

    setIsConnected(false);
}

bool BleInterface::disconnectDevice(QString address) {
    bool result = false;

    if(address != "") {
        QBluetoothAddress a(address);
        ControllerInstance *instance = bleControllers.value(a);

        if(instance != Q_NULLPTR) {
            instance->controller->disconnectFromDevice();
        }
    }

    setIsConnected(false);
    return result;
}

void BleInterface::setStreamsEnabled(QLowEnergyService *service, QBluetoothUuid uuid, bool enabled) {
    QLowEnergyCharacteristic characteristic;
    characteristic = service->characteristic(uuid);

    if(characteristic.isValid()) {
        QLowEnergyDescriptor notifDescriptor = characteristic.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        setStreamEnabled(service, &notifDescriptor, enabled);
        connect(service, &QLowEnergyService::characteristicChanged, this, &BleInterface::characteristicUpdated);
    }
}

void BleInterface::setStreamEnabled(QLowEnergyService *service, QLowEnergyDescriptor *descr, bool enabled) {
    if(service && descr->isValid()) {
        QByteArray data = enabled ? QByteArray::fromHex("0100") : QByteArray::fromHex("0000");
        service->writeDescriptor(*descr, data);
    }
}

QStringList BleInterface::getDevicesNames() const {
    return btDevices.keys();
}

bool BleInterface::isConnected() {
    return connected;
}

void BleInterface::deviceFound(const QBluetoothDeviceInfo &info) {
    if(info.name().contains("Silvia BLE")) {
        btDevices.insert(info.name(), info);
        //qDebug() << "*******found BLE Device: " << info.name() << " - " << info.address();
        bool autoConnect = true;

        if(autoConnect) {
            connectDevice(info.name());
        }
    }
}

void BleInterface::scanFinished() {
    qDebug() << "found " << btDevices.size() << " devices:";

    if(btDevices.empty()) {
        startDiscovery();
    }
}
